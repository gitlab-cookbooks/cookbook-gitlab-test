# Install runner and docker

file "/etc/apt/sources.list.d/runner_gitlab-ci-multi-runner.list" do
  content "deb https://packages.gitlab.com/runner/gitlab-ci-multi-runner/#{node['platform']}/ #{node['lsb']['codename']} main\ndeb-src https://packages.gitlab.com/runner/gitlab-ci-multi-runner/#{node['platform']}/ #{node['lsb']['codename']} main"
  notifies :run, "bash[setup multi runner]", :immediately
end

bash "setup multi runner" do
  code <<-EOH
    curl https://packages.gitlab.com/gpg.key | sudo apt-key add -
    apt-get update
  EOH
  action :nothing
end

package 'gitlab-ci-multi-runner'

package 'curl'

# Install Docker quick and dirty
execute 'curl -sSL https://get.docker.com/ | sh' do
  not_if 'which docker'
end

# Restart Docker after iptables-persistent for nat rules
template '/etc/rc.local' do
  owner  'root'
  group  'root'
  mode   '0755'
end
