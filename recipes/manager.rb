# taken from https://github.com/docker/machine/releases/tag/v0.6.0
remote_file '/usr/bin/docker-machine' do
  source 'https://github.com/docker/machine/releases/download/v0.6.0/docker-machine-Linux-x86_64'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
  checksum '6c383c4716985db2d7ae7e1689cc4acee0b23284e6e852d6bc59011696ca734a'
end

cookbook_file '/usr/lib/check_mk_agent/local/docker_machine.sh' do
  owner 'root'
  group 'root'
  mode '0755'
end
