#
# Cookbook Name:: cookbook-gitlab-test
# Recipe:: default
#
# Copyright (C) 2015 GitLab B.V.
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'cookbook-gitlab-test::git'

include_recipe 'cookbook-gitlab-test::runner'
