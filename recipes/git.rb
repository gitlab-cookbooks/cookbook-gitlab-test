# Install Git

git_version = node['cookbook-gitlab-test']['git_version']
git_tarball = node['cookbook-gitlab-test']['git_tarball']
git_url = "https://www.kernel.org/pub/software/scm/git/#{git_tarball}"
git_tarball_sha256 = node['cookbook-gitlab-test']['git_tarball_sha256']
local_git_tarball = "/var/cache/#{git_tarball}"

packages = %w{
  gcc libcurl4-gnutls-dev libexpat1-dev gettext
  libz-dev libssl-dev make
}

packages.each do |pkg|
  package pkg
end

remote_file local_git_tarball do
  source git_url
  checksum git_tarball_sha256
end

bash "install git #{git_version}" do
  code <<-EOS
set -e # Exit on failure
cd /var/cache
tar xzf #{local_git_tarball}
cd /var/cache/git-#{git_version}
make prefix=/usr/local install
EOS
  not_if "/usr/local/bin/git --version | grep '#{git_version}'"
end
