name             'cookbook-gitlab-test'
maintainer       'GitLab B.V.'
maintainer_email 'jacob@gitlab.com'
license          'MIT'
description      'Installs/Configures cookbook-gitlab-test'
long_description 'Installs/Configures cookbook-gitlab-test'
version          '0.2.5'
