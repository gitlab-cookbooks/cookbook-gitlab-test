default['cookbook-gitlab-test']['git_version'] = '2.7.4'
default['cookbook-gitlab-test']['git_tarball'] = "git-#{node['cookbook-gitlab-test']['git_version']}.tar.gz"
default['cookbook-gitlab-test']['git_tarball_sha256'] = '7104c4f5d948a75b499a954524cb281fe30c6649d8abe20982936f75ec1f275b'
