# cookbook-gitlab-test-cookbook

This cookbook installs Docker and gitlab-runner on an Ubuntu 14.04
machine to run the GitLab test suite using GitLab CI.

This cookbook should probably be deprecated.

## Registering a runner

After running this recipe on a node you still need to manually register the new
node with your GitLab CI coordinator. You can use the following interactive
command for that:

```
sudo gitlab-runner register
```

## Supported Platforms

Ubuntu 14.04, Ubuntu 12.04

## License and Authors

Author:: Jacob Vosmaer (jacob@gitlab.com)
License:: MIT
