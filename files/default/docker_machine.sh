#!/bin/bash

error_warning_level=35
error_critical_level=50

machines=$(ls -l /root/.docker/machine/machines/ | grep ^d | wc -l)
error_machines=0
for machinie in /root/.docker/machine/machines/*/config.json; do
    grep '"DropletID": 0' $machinie && error_machines=$[$error_machines + 1]
done

echo "0 Docker_machine dockers=$machines;;;0;|error_machines=$error_machines;$error_warning_level;$error_critical_level;0; OK - $machines docker machines running"
